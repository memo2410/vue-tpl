import Vue from 'vue'
import Router from 'vue-router'
import inicio from '@/components/inicio'
import bancos from '@/components/bancos'
import inmuebles from '@/components/inmuebles'
import seguros from '@/components/seguros'
import negocios from '@/components/negocios'
import vehiculos from '@/components/vehiculos'
import otros from '@/components/otros'
import cartas from '@/components/cartas'
import fotos from '@/components/fotos'
import video from '@/components/video'
import documentos from '@/components/documentos'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'inicio',
      component: inicio
    },
    {
      path: '/bancos',
      name: 'bancos',
      component: bancos
    },
    {
      path: '/inmuebles',
      name: 'inmuebles',
      component: inmuebles
    },
    {
      path: '/seguros',
      name: 'seguros',
      component: seguros
    },
    {
      path: '/negocios',
      name: 'negocios',
      component: negocios
    },
    {
      path: '/vehiculos',
      name: 'vehiculos',
      component: vehiculos
    },
    {
      path: '/otros',
      name: 'otros',
      component: otros
    },
    {
      path: '/cartas',
      name: 'cartas',
      component: cartas
    },
    {
      path: '/fotos',
      name: 'fotos',
      component: fotos
    },
    {
      path: '/video',
      name: 'video',
      component: video
    },
    {
      path: '/documentos',
      name: 'documentos',
      component: documentos
    }
  ]
})
